package com.kshrd.eventwebill.view.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public class BaseActivity <T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity {

    /**
     * handle all view data binding
     */
    protected ViewDataBinding mViewDataBinding;

    /**
     * handle all view model
     */
    protected BaseViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * @param layoutId        Layout ID
     * @param viewModel       View Model
     * @param bindingVariable Binding Variable
     */
    protected void setContentViewBinding(int layoutId, BaseViewModel viewModel, int bindingVariable) {
        this.mViewDataBinding = DataBindingUtil.setContentView(this, layoutId);
        this.mViewModel = viewModel;
        this.mViewDataBinding.setVariable(bindingVariable, this.mViewModel);
    }
}

