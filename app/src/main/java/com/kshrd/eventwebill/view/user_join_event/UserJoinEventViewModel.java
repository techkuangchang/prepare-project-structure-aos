package com.kshrd.eventwebill.view.user_join_event;

import android.app.Application;

import androidx.annotation.NonNull;

import com.kshrd.eventwebill.view.base.BaseViewModel;
import com.kshrd.retrofitdatabindinglibrary.comm.connection.ComTran;
import com.kshrd.retrofitdatabindinglibrary.comm.network.RetrofitNetwork;

public class UserJoinEventViewModel extends BaseViewModel implements ComTran.OnComTranListener {

    public UserJoinEventViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void onTranResponse(String tranCd, Object object) {

    }

    @Override
    public void onTranError(String tranCd, Object object) {

    }
}
