package com.kshrd.eventwebill.view.user;

import android.app.Application;

import androidx.annotation.NonNull;

import com.kshrd.eventwebill.view.base.BaseViewModel;
import com.kshrd.retrofitdatabindinglibrary.comm.connection.ComTran;

public class UserViewModel extends BaseViewModel implements ComTran.OnComTranListener {

    public UserViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void onTranResponse(String tranCd, Object object) {

    }

    @Override
    public void onTranError(String tranCd, Object object) {

    }
}
