package com.kshrd.eventwebill.view.payhistory;

import android.app.Application;

import androidx.annotation.NonNull;

import com.kshrd.eventwebill.view.base.BaseViewModel;
import com.kshrd.retrofitdatabindinglibrary.comm.connection.ComTran;

public class HistoryPayViewModel extends BaseViewModel implements ComTran.OnComTranListener {

    public HistoryPayViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void onTranResponse(String tranCd, Object object) {

    }

    @Override
    public void onTranError(String tranCd, Object object) {

    }
}