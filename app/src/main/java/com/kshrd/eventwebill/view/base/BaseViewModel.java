package com.kshrd.eventwebill.view.base;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.kshrd.eventwebill.model.error.HandleError;

/**
 * @since 2020.12.28
 * @author chanyouvitagmail.com
 */
public class BaseViewModel extends AndroidViewModel {

    /**
     * handle call back function when error
     */
    protected MutableLiveData<HandleError> mErrorLiveData;

    /**
     * handle call back function loading view
     */
    protected MutableLiveData<Boolean> mHandleLoading;

    /**
     * handle call back function dialog error
     */
    protected MutableLiveData<Boolean> dialogErrorShowing;

    /**
     * init handle error object
     */
    protected HandleError mHandleError;

    public BaseViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<HandleError> getHandleError() {
        return mErrorLiveData;
    }

    public MutableLiveData<Boolean> getHandleLoading() {
        return mHandleLoading;
    }

    public MutableLiveData<Boolean> getDialogErrorShowing() {
        return dialogErrorShowing;
    }

}
