package com.kshrd.eventwebill.model.error;

/**
 * @since 2019.12.02
 * @author KOSIGN-SMART
 *
 * Useful handle error of server respond
 * get no error message respond success, has error message respond fail
 */
public class HandleError {

    private String mErorMessage;

    private String mTranCode;

    /**
     * get error message
     */
    public String getErrorMessage() {
        return mErorMessage;
    }

    /**
     * set error message
     */
    public void setErrorMessage(String errorMessage) {
        this.mErorMessage = errorMessage;
    }

    /**
     * get service api code
     */
    public String getTranCode() {
        return mTranCode;
    }

    /**
     * set service api code
     */
    public void setTranCode(String tranCode) {
        this.mTranCode = tranCode;
    }

}
