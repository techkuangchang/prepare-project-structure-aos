package com.kshrd.retrofitdatabindinglibrary.comm.network;

import android.content.Context;
import android.os.Build;
import android.webkit.CookieSyncManager;

import androidx.annotation.IntDef;
import androidx.annotation.StringDef;

import com.kshrd.retrofitdatabindinglibrary.comm.network.internal.OnNetworkListener;
import com.kshrd.retrofitdatabindinglibrary.comm.network.retrofit.PersistentCookieStore;
import com.kshrd.retrofitdatabindinglibrary.comm.network.retrofit.RetrofitService;
import com.kshrd.retrofitdatabindinglibrary.comm.network.retrofit.RetrofitUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * RetrofitNetwork
 *
 * @author chan youvita
 * @since 2019. 11. 27.
 **/
public class RetrofitNetwork {

    /**
     * Cookie
     */
    private CookieManager cookieManager;
    private PersistentCookieStore mCookieStore;

    /**
     * Retrofit
     */
    private Retrofit mRetrofit = null;

    /**
     *
     */
    private Call<ResponseBody> mRequest;

    /**
     * Context
     */
    private Context mContext;

    /**
     * default connect time out
     */
    private static int mTimeOut = 60;

    /**
     * default read time out
     */
    private static int mReadTimeOut = 30;

    /**
     * 공통 Headers
     */
    private Map<String, String> mHeaders = null;

    /**
     * Charset (default = UTF-8)
     */
    private Charset CHARSET = Charset.forName("UTF-8");

    /**
     * Network Listener
     */
    private OnNetworkListener mOnNetworkListener;

    /**
     * Request code
     */
    private String mTranCode;

    /**
     * Request method
     */
    public static final int METHOD_POST   = 0;
    public static final int METHOD_PATCH  = 1;
    public static final int METHOD_PUT    = 2;
    @IntDef(value = {
            METHOD_POST,
            METHOD_PATCH,
            METHOD_PUT
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface Method {}

    /**
     * Request multi part method
     */
    public static final String MULTI_PART_POST   = "POST";
    public static final String MULTI_PART_PUT    = "PUT";
    @StringDef(value = {
            MULTI_PART_POST,
            MULTI_PART_PUT
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface MultiPartMethod {}

    /**
     * @param context Context
     * @param onNetworkListener 통신 Callback 리스너 ( OnNetworkListener )
     */
    public RetrofitNetwork(Context context, OnNetworkListener onNetworkListener) {
        mContext = context;
        mOnNetworkListener = onNetworkListener;
        mCookieStore = new PersistentCookieStore(context);

        initRequestHeader();
    }

    /**
     * 공통 Headers 설정 <br>
     */
    public void setComHeaders(Map<String, String> headers) {
        mHeaders = headers;
    }

    /**
     * Header Request 초기화 <br>
     */
    private void initRequestHeader() {
        try {
            // TODO 공통 Header 기본 세팅
            mHeaders = new HashMap<String, String>();
            mHeaders.put("charset", CHARSET.name());
            mHeaders.put("Content-Type", "application/x-www-form-urlencoded; charset="+CHARSET.name());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CookieManager getCookieManager() {
        return cookieManager;
    }

    /**
     * set time out
     * @param timeOut amount timeout
     */
    public void setTimeOut(int timeOut) {
        mTimeOut = timeOut;
    }

    /**
     * set time out
     * @param readTimeOut amount timeout
     */
    public void setReadTimeOut(int readTimeOut) {
        mReadTimeOut = readTimeOut;
    }

    /**
     * init retrofit client with session, log message from retrofit
     */
    private Retrofit getClient(String baseUrl) {
        /*
         * init cookie manager
         */
        cookieManager = new CookieManager(mCookieStore, CookiePolicy.ACCEPT_ORIGINAL_SERVER);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .retryOnConnectionFailure(true)
                .addInterceptor(logging)
                .connectTimeout(mTimeOut, TimeUnit.SECONDS)
                .readTimeout(mReadTimeOut, TimeUnit.SECONDS)
                .build();

        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl + "/")
                    .client(client)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(RetrofitUtils.gson))
                    .build();
        }
        return mRetrofit;
    }

    /**
     * get service retrofit to ues api request & respond
     */
    private RetrofitService getService(String baseUrl) {
        return getClient(baseUrl).create(RetrofitService.class);
    }

    /**
     * @param tranCode :  HTTP 전문 코드
	 * @param postType : HTTP 통신 타입 (true : POST , false : GET)
	 * @param url : HTTP URL
	 * @param param : request 전문 data (Map<String,String>)
	 */
    public void requestRetrofitNetwork(final String tranCode, final boolean postType, final String url, final Map<String, String> param) {
        try {
            mTranCode = tranCode;

            RetrofitService service = getService(url);
            mRequest = postType ? service.postRequestMessage(mHeaders, url, param) : service.getRequestMessage(mHeaders, url, param);
            mRequest.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            if (response.body() != null) {
                                mOnNetworkListener.onNetworkResponse(tranCode, response.body().string());
                            }
                        } catch (Exception e) {
                            e.getStackTrace();
                            mOnNetworkListener.onNetworkError(tranCode, e);
                        }
                    }else {
                        try {
                            if (response.errorBody() != null) {
                                mOnNetworkListener.onNetworkError(tranCode, response.errorBody().string());
                            }
                        }catch (Exception e) {
                            e.printStackTrace();
                            mOnNetworkListener.onNetworkError(tranCode, e);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mRequest = call;
                    mOnNetworkListener.onNetworkError(tranCode, t);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param tranCode :  HTTP 전문 코드
     * @param url : HTTP URL
     * @param param : request 전문 data (Map<String,String>)
     */
    public void requestRetrofitNetwork(final String tranCode, final String url, final Map<String, String> param) {
        try {
            mTranCode = tranCode;

            RetrofitService service = getService(url);
            mRequest = service.postRequestMessageBody(mHeaders, url, param);
            mRequest.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            if (response.body() != null) {
                                mOnNetworkListener.onNetworkResponse(tranCode, response.body().string());
                            }
                        } catch (Exception e) {
                            e.getStackTrace();
                            mOnNetworkListener.onNetworkError(tranCode, e);
                        }
                    }else {
                        try {
                            if (response.errorBody() != null) {
                                mOnNetworkListener.onNetworkError(tranCode, response.errorBody().string());
                            }
                        }catch (Exception e) {
                            e.printStackTrace();
                            mOnNetworkListener.onNetworkError(tranCode, e);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mRequest = call;
                    mOnNetworkListener.onNetworkError(tranCode, t);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param tranCode :  HTTP 전문 코드
     * @param url : HTTP URL
     * @param param : request 전문 data (Map<String,String>)
     */
    public void requestRetrofitNetwork(final String tranCode, @Method int method, final String url, final String param) {
        try {
            mTranCode = tranCode;
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), param);
            RetrofitService service = getService(url);
            switch (method) {
                case METHOD_POST:
                    mRequest = service.postRequestMessageBody(mHeaders, url, requestBody);
                    break;

                case METHOD_PATCH:
                    mRequest = service.patchRequestMessageBody(mHeaders, url, requestBody);
                    break;

                case METHOD_PUT:
                    mRequest = service.putRequestMessageBody(mHeaders, url, requestBody);
                    break;
            }

            mRequest.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            if (response.body() != null) {
                                mOnNetworkListener.onNetworkResponse(tranCode, response.body().string());
                            }
                        } catch (Exception e) {
                            e.getStackTrace();
                            mOnNetworkListener.onNetworkError(tranCode, e);
                        }
                    }else {
                        try {
                            if (response.errorBody() != null) {
                                mOnNetworkListener.onNetworkError(tranCode, response.errorBody().string());
                            }
                        }catch (Exception e) {
                            e.printStackTrace();
                            mOnNetworkListener.onNetworkError(tranCode, e);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mRequest = call;
                    mOnNetworkListener.onNetworkError(tranCode, t);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param tranCode :  HTTP 전문 코드
     * @param url : HTTP URL
     */
    public void requestRetrofitNetwork(final String tranCode, @MultiPartMethod String method, final String url, Map<String, MultipartBody> param) {
        try {
            RetrofitService service = getService(url);
            mRequest = method.equals(MULTI_PART_POST) ? service.postRequestMultipart(mHeaders, url, param) : service.putRequestMultipart(mHeaders, url, param);
            mRequest.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            if (response.body() != null) {
                                mOnNetworkListener.onNetworkResponse(tranCode, response.body().string());
                            }
                        } catch (Exception e) {
                            e.getStackTrace();
                            mOnNetworkListener.onNetworkError(tranCode, e);
                        }
                    }else {
                        try {
                            if (response.errorBody() != null) {
                                mOnNetworkListener.onNetworkError(tranCode, response.errorBody().string());
                            }
                        }catch (Exception e) {
                            e.printStackTrace();
                            mOnNetworkListener.onNetworkError(tranCode, e);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mRequest = call;
                    mOnNetworkListener.onNetworkError(tranCode, t);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * retry request data connection
     */
    public void retry() {
        mRequest.clone().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            mOnNetworkListener.onNetworkResponse(mTranCode, response.body().string());
                        }
                    } catch (Exception e) {
                        e.getStackTrace();
                        mOnNetworkListener.onNetworkError(mTranCode, e);
                    }
                }else {
                    try {
                        if (response.errorBody() != null) {
                            mOnNetworkListener.onNetworkError(mTranCode, response.errorBody().string());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        mOnNetworkListener.onNetworkError(mTranCode, e);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mOnNetworkListener.onNetworkError(mTranCode, t);
            }
        });
    }

    /**
     * - Build.VERSION_CODES.LOLLIPOP 기준으로 쿠키 동기화 구분 처리
     *
     * @param domain : 도메인 uri
     */
    public void syncSessionCookies(String domain) {
        try {
            android.webkit.CookieManager cookieManager = android.webkit.CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);

            URI uri = null;
            try {
                uri = new URI(domain);
            } catch (URISyntaxException e) { e.printStackTrace(); }
            String url = uri.toString();

            List<HttpCookie> cookies = mCookieStore.getCookies();
            for (HttpCookie cookie : cookies) {
                String setCookie = new StringBuilder(cookie.toString())
                        .append("; domain=").append(cookie.getDomain())
                        .append("; path=").append(cookie.getPath())
                        .toString();
                cookieManager.setCookie(url, setCookie);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                android.webkit.CookieManager.getInstance().flush();
            } else {
                try {
                    CookieSyncManager.getInstance();
                } catch (Exception e) {
                    CookieSyncManager.createInstance(mContext);
                }
                CookieSyncManager.getInstance().sync();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
