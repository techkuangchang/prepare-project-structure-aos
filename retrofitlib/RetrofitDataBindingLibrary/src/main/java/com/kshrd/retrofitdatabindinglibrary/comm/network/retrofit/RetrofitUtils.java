package com.kshrd.retrofitdatabindinglibrary.comm.network.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Useful to convert object data from server to gson
 * and get service retrofit to request server api
 *
 * @author chan youvita
 * @since 2019. 11. 20.
 */
public class RetrofitUtils {

    /**
     * init gson
     */
    public static Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
            .setLenient()
            .create();

    /**
     * convert object to gson object
     * @param object object type respond from server
     * @param cn class respond object type of gson
     */
    public static <T> T jsonParser(Object object, Class<T> cn) {
        return gson.fromJson(String.valueOf(object), cn);
    }

    /**
     * convert array list to string
     * @param list list item
     */
    public static String arrayParser(ArrayList<?> list) {
        return new Gson().toJson(list);
    }

    /**
     * convert string to array list
     */
    public static <T> T arrayList(String value, Type type) {
        return new Gson().fromJson(value, type);
    }

    /**
     * convert array list to string with expose serialize.
     * @param list list item
     */
    public static String arrayParserExpose(ArrayList<?> list) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(list);
    }

    /**
     * register adapter null string
     */
    public static class NullStringToEmptyAdapterFactory<T> implements TypeAdapterFactory {
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

            Class<T> rawType = (Class<T>) type.getRawType();
            if (rawType != String.class) {
                return null;
            }
            return (TypeAdapter<T>) new StringAdapter();
        }
    }

    /**
     * convert field null to empty string
     */
    public static class StringAdapter extends TypeAdapter<String> {
        public String read(JsonReader reader) throws IOException {
            if (reader.peek() == JsonToken.NULL) {
                reader.nextNull();
                return "";
            }
            return reader.nextString();
        }
        public void write(JsonWriter writer, String value) throws IOException {
            if (value == null) {
                writer.nullValue();
                return;
            }
            writer.value(value);
        }
    }
}